resource "openstack_compute_quotaset_v2" "quotaset_1" {
  project_id           = var.adm_project_id
  key_pairs            = 10
  ram                  = 300960
  cores                = 100
  instances            = 20
}