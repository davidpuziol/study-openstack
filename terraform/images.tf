resource "openstack_images_image_v2" "rocky" {
  name             = "rocky-8.9"
  container_format = "bare"
  disk_format      = "qcow2"
  image_source_url = "https://download.rockylinux.org/pub/rocky/8/images/x86_64/Rocky-8-GenericCloud-Base-8.9-20231119.0.x86_64.qcow2"
  web_download = true
  visibility = "public"
}

resource "openstack_images_image_v2" "ubuntu" {
  name             = "ubuntu-22.04"
  container_format = "bare"
  disk_format      = "qcow2"
  image_source_url = "https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64.img"
  web_download = true
  visibility = "public"
}

resource "openstack_images_image_v2" "centos" {
  name             = "centos-7"
  container_format = "bare"
  disk_format      = "qcow2"
  image_source_url = "https://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud.qcow2c"
  web_download = true
  visibility = "public"
}
