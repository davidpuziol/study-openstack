resource "tls_private_key" "default_keypair" {
  algorithm   = "RSA"
  rsa_bits    = 2048
}

resource "openstack_compute_keypair_v2" "default_keypair" {
  name       = var.key_name
  public_key = tls_private_key.default_keypair.public_key_openssh
}

resource "null_resource" "save_private_key" {
  provisioner "local-exec" {
    command = <<-EOT
      mkdir -p keys &&
      echo '${tls_private_key.default_keypair.private_key_pem}' > keys/${var.key_name}.pem &&
      echo '${tls_private_key.default_keypair.public_key_openssh}' > keys/${var.key_name}.pub &&
      chmod 700  keys/${var.key_name}.pem && 
      chmod 700  keys/${var.key_name}.pub
    EOT
  }
}