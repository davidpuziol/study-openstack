variable openstack_user_name {
description = "value"
type = string
}

variable openstack_password {
description = "value"
type = string
}

variable openstack_auth_url {
description = "value"
type = string
}

variable openstack_tenant_name {
description = "value"
type = string   
}

variable nodes {
  description = "Fixed nodes to run"
  type           = map(object({
    image_name   = string
    flavor_name    = string
  }))
  default = {}
}

variable "adm_project_id" {
  description = "Admin Project ID"
  type = string
}

variable "image_info" {
  description = "Mapa de informações sobre as imagens"
  type           = map(object({
    url          = string
    version      = string
    architecture = string
    ostype       = string
    visibility   = string
  }))

  default = {}
}

variable "vpc_cidr"{
  description = "VPC CIDR"
  default = "147.11.123.0/24"
}

variable "subnet_start_number"{
  description = "Allocation Pool START Number for public subnet into default vpc"
  type = number
  default = 50
}

variable "subnet_end_number"{
  description = "Allocation Pool END Number for public subnet into default vpc"
  type = number
  default = 110
}

variable "nameservers" {
  description = "DNS Nameservers"
  type = list(string)
  default = ["8.8.8.8", "1.1.1.1"]  
}

variable "key_name" {
  description = "Key Name"
  type = string
  default = "default"
}

variable "node_image_cluster" {
  description = "Image used by masters and workers"
  type = string
  default = "rocky"
}