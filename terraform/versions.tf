terraform {
required_version = ">= 1.5.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
    external = {
      source  = "hashicorp/external"
      version = "~> 2.3.2"
    }
  }
  
}