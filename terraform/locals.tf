locals {

  cluster_image_id = var.node_image_cluster == "ubuntu" ? openstack_images_image_v2.ubuntu.id : openstack_images_image_v2.rocky.id

  user_data = <<-EOF
#!/bin/bash
echo '${var.node_image_cluster}:${var.node_image_cluster}' | chpasswd
EOF
}