resource "openstack_compute_instance_v2" "masters" {
  count = 3

  name            = "master-${count.index + 1}"
  image_id        = local.cluster_image_id
  flavor_id       = openstack_compute_flavor_v2.small.id
  key_pair        = var.key_name
  security_groups = ["default"]

  user_data = local.user_data
  network {
    name = openstack_networking_network_v2.vpc.name
    fixed_ip_v4 = cidrhost(var.vpc_cidr, (count.index + 1 + var.subnet_start_number))
  }

  block_device {
    uuid                  = local.cluster_image_id
    source_type           = "image"
    volume_size           = openstack_compute_flavor_v2.small.disk
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  provisioner "file" {
    source      = "harbor_certs/certs.tar.gz"
    destination = "/home/${var.node_image_cluster}/certs.tar.gz"
    connection {
      type        = "ssh"
      user        = var.node_image_cluster
      host        = self.network.0.fixed_ip_v4
      private_key = tls_private_key.default_keypair.private_key_pem
    }
  }

  provisioner "remote-exec" {
    inline = var.node_image_cluster == "rocky" ? [
      "sudo tar zxfv /home/${var.node_image_cluster}/certs.tar.gz -C /etc/pki/ca-trust/source/anchors/",
      "sudo update-ca-trust",
    ]:[
      "sudo tar zxfv /home/${var.node_image_cluster}/certs.tar.gz -C /etc/ssl/certs/",
      "sudo apt-get install -y ca-certificates",
      "sudo update-ca-certificates",
    ]
     connection {
      type        = "ssh"
      user        = var.node_image_cluster
      host        = self.network.0.fixed_ip_v4
      private_key = tls_private_key.default_keypair.private_key_pem
    }

  }
}


####################################################################################################

resource "openstack_compute_instance_v2" "ingress" {

  name            = "ingress"
  image_id        = local.cluster_image_id
  flavor_id       = openstack_compute_flavor_v2.small.id
  key_pair        = var.key_name
  security_groups = ["default"]

  user_data = local.user_data
  network {
    name = openstack_networking_network_v2.vpc.name
    fixed_ip_v4 = cidrhost(var.vpc_cidr, (10 + var.subnet_start_number))
  }

  block_device {
    uuid                  = local.cluster_image_id
    source_type           = "image"
    volume_size           = openstack_compute_flavor_v2.small.disk
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  provisioner "file" {
    source      = "harbor_certs/certs.tar.gz"
    destination = "/home/${var.node_image_cluster}/certs.tar.gz"
    connection {
      type        = "ssh"
      user        = var.node_image_cluster
      host        = self.network.0.fixed_ip_v4
      private_key = tls_private_key.default_keypair.private_key_pem
    }
  }

  provisioner "remote-exec" {
    inline = var.node_image_cluster == "rocky" ? [
      "sudo tar zxfv /home/${var.node_image_cluster}/certs.tar.gz -C /etc/pki/ca-trust/source/anchors/",
      "sudo update-ca-trust",
    ]:[
      "sudo tar zxfv /home/${var.node_image_cluster}/certs.tar.gz -C /etc/ssl/certs/",
      "sudo apt-get install -y ca-certificates",
      "sudo update-ca-certificates",
    ]

    connection {
      type        = "ssh"
      user        = var.node_image_cluster
      host        = self.network.0.fixed_ip_v4
      private_key = tls_private_key.default_keypair.private_key_pem
    }
  }
}

###################################################################################################

resource "openstack_compute_instance_v2" "workers" {
  count = 3

  name            = "worker-${count.index + 1}"
  image_id        = local.cluster_image_id
  flavor_id       = openstack_compute_flavor_v2.large.id
  key_pair        = var.key_name
  security_groups = ["default"]

  user_data = local.user_data

  network {
    name = openstack_networking_network_v2.vpc.name
    fixed_ip_v4 = cidrhost(var.vpc_cidr, (count.index + 11 + var.subnet_start_number))
  }

  block_device {
    uuid                  = local.cluster_image_id
    source_type           = "image"
    volume_size           = openstack_compute_flavor_v2.large.disk
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  provisioner "file" {
    source      = "harbor_certs/certs.tar.gz"
    destination = "/home/${var.node_image_cluster}/certs.tar.gz"
    connection {
      type        = "ssh"
      user        = var.node_image_cluster
      host        = self.network.0.fixed_ip_v4
      private_key = tls_private_key.default_keypair.private_key_pem
    }
  }

  provisioner "remote-exec" {
    inline = var.node_image_cluster == "rocky" ? [
      "sudo tar zxfv /home/${var.node_image_cluster}/certs.tar.gz -C /etc/pki/ca-trust/source/anchors/",
      "sudo update-ca-trust",
    ]:[
      "sudo tar zxfv /home/${var.node_image_cluster}/certs.tar.gz -C /etc/ssl/certs/",
      "sudo apt-get install -y ca-certificates",
      "sudo update-ca-certificates",
    ]

    connection {
      type        = "ssh"
      user        = var.node_image_cluster
      host        = self.network.0.fixed_ip_v4
      private_key = tls_private_key.default_keypair.private_key_pem
    }
  }
}

##################################################################################################


resource "openstack_compute_instance_v2" "conductor" {

  name            = "conductor"
  image_id        = openstack_images_image_v2.rocky.id
  flavor_id       = openstack_compute_flavor_v2.medium.id
  key_pair        = var.key_name
  security_groups = ["default"]

  network {
    name = openstack_networking_network_v2.vpc.name
    fixed_ip_v4 = cidrhost(var.vpc_cidr, (var.subnet_start_number + 50))
  }

  block_device {
    uuid                  = openstack_images_image_v2.rocky.id
    source_type           = "image"
    volume_size           = openstack_compute_flavor_v2.medium.disk
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  user_data = local.user_data

  provisioner "file" {
    source      = "harbor_certs/certs.tar.gz"
    destination = "/home/rocky/certs.tar.gz"
    connection {
      type        = "ssh"
      user        = "rocky"
      host        = self.network.0.fixed_ip_v4
      private_key = tls_private_key.default_keypair.private_key_pem
    }
  }

  provisioner "remote-exec" {
    inline = [
      "sudo tar zxfv /home/rocky/certs.tar.gz -C /etc/pki/ca-trust/source/anchors/",
      "sudo update-ca-trust",
    ]

    connection {
      type        = "ssh"
      user        = "rocky"
      host        = self.network.0.fixed_ip_v4
      private_key = tls_private_key.default_keypair.private_key_pem
    }
  }
}