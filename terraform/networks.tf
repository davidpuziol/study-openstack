resource "openstack_networking_network_v2" "vpc" {
  
  name           = "vpc-default"
  admin_state_up = "true"
  shared         = true
  external       = true
  mtu            = 1500
  segments {
    network_type   = "flat"
    physical_network = "vlan"
  }
}

resource "openstack_networking_subnet_v2" "public_subnet" {
  
  description = "Default Public Subnet"
  name       = "public-subnet-default"
  network_id = openstack_networking_network_v2.vpc.id
  cidr       = var.vpc_cidr
  gateway_ip = cidrhost(var.vpc_cidr, 1)
  ip_version = 4
  enable_dhcp = true
  dns_nameservers = var.nameservers
  allocation_pool {
    start = cidrhost(var.vpc_cidr, var.subnet_start_number)
    end   = cidrhost(var.vpc_cidr, var.subnet_end_number)
  }
}