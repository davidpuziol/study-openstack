resource "openstack_compute_flavor_v2" "small" {
  name  = "small"
  vcpus = 4
  ram   = 4096
  disk  = 20
  swap  = 0
  is_public = true
}

resource "openstack_compute_flavor_v2" "medium" {
  name  = "medium"
  vcpus = 8
  ram   = 16384
  disk  = 100
  swap  = 0
  is_public = true
}

resource "openstack_compute_flavor_v2" "large" {
  name  = "large"
  vcpus = 16
  ram   = 65536
  disk  = 40
  swap  = 0
  is_public = true
}