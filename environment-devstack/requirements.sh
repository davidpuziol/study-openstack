#!/bin/bash

sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get dist-upgrade -y

sudo useradd -s /bin/bash -d /opt/stack -m stack
sudo chmod +x /opt/stack
sudo echo "stack ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers.d/stack
sudo -u stack -i
git clone -b stable/2023.2 https://opendev.org/openstack/devstack.git /opt/stack/devstack

chown stack:stack -R /opt/stack