#!/bin/bash

sudo -u stack -i

cd /opt/stack/devstack

echo "Creating Config local.conf"

MY_HOST_IP=$(ip addr show enp0s8 | grep inet | head -n 1 | awk '{print $2}' | awk -F'/' '{print $1}')
MY_FLOATING_RANGE=$(echo $MY_HOST_IP | awk -F '.' '{print $1"."$2"."$3".224/27"}')
MY_FIXED_RANGE=$(echo $MY_HOST_IP | awk -F '.' '{print $1".10.10.0/24"}')

MY_ADMIN_PASSWORD=admin
MY_DEST=/opt/stack

echo "#Personal Config
[[local|localrc]]
ADMIN_PASSWORD=$MY_ADMIN_PASSWORD
DATABASE_PASSWORD=$MY_ADMIN_PASSWORD
RABBIT_PASSWORD=$MY_ADMIN_PASSWORD
SERVICE_PASSWORD=$MY_ADMIN_PASSWORD
FIXED_NETWORK_SIZE=256
DEST=$MY_DEST

LOGDAYS=2
LOGFILE=$MY_DEST/logs/stack.sh.log

HOST_IP=$MY_HOST_IP

FLAT_INTERFACE=enp0s8
FLOATING_RANGE=$MY_FLOATING_RANGE
FIXED_RANGE=$MY_FIXED_RANGE

SWIFT_REPLICAS=1
SWIFT_DATA_DIR=$MY_DEST/data
#Check https://docs.openstack.org/devstack/latest/configuration.html" > local.conf

chown stack:stack -R /opt/stack