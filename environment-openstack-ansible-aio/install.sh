#!/bin/bash

sudo su

sudo apt-get update
sudo apt-get upgrade -y
# sudo apt-get dist-upgrade -y

git clone https://opendev.org/openstack/openstack-ansible \
    /opt/openstack-ansible
cd /opt/openstack-ansible

# Se precisar de mudar a branch
#git checkout <branchname>

scripts/bootstrap-ansible.sh

export BOOTSTRAP_OPTS="bootstrap_host_data_disk_device=sda"
export BOOTSTRAP_OPTS="bootstrap_host_public_interface=enp0s8"

scripts/bootstrap-aio.sh

openstack-ansible setup-hosts.yml
openstack-ansible setup-infrastructure.yml
openstack-ansible setup-openstack.yml

grep admin_pass /etc/openstack_deploy/user_secrets.yml
