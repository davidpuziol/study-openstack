# Dashboard Horizon

Uma vez instalado o openstack podemos acessar o dashboard pelo ip da máquina onde o openstack foi instalado. O componente que provê esta interface gráfica é o Horizon. Esse é só um overview geral básico para trazer familiaridade com o openstack.

Veja qual o ip da máquina do openstack e acesse para ver a interface inicial.

![dashboard](./pics/dashboard.jpg)

Fazendo o login, provavelmente virá tudo em português, mas você pode trocar o idioma indo nas configurações do usuário.

![config](./pics/config.jpg)

![language](./pics/language.jpg)

Iniciando as explicações, a parte de identify do dashboard terá acesso aos recursos do `Keystone``.

![keystone-dash](./pics/keystone-dash.jpg)

O domínio é a parte de autenticação de nível mais alto e em dentro de um domínio você terá os usuários, projectos etc.

![users-dash](./pics/users-dash.jpg)
![projects-dash](./pics/projects-dash.jpg)

O domínio se fossemos comparar com a AWS está para as organizações. Se tivessemos duas empresas diferentes usando mesmo openstack por algum motivo, podemos separar os usuários por exemplo pelo domínio.

Os projetos são basicamente para você isolar os recursos do openstack, por exemplo por departamento da empresa, por ambiente de desenvolvimento e produção, etc. Cada projeto possuem os usuários que podem iteratir com o projeto.

Por exemplo o usuário admin possui permissão para o projeto admin e demo, mas o que ele criar no projeto admin estará no projeto admin e o projeto demo não terá visibilidade ao recursos criados.

![admin](./pics/projects-users.jpg)

Os grupos funcionaram para dar permissões específicas para todos os usuários que estivem no mesmo grupo de uma única vez.

As roles são justamentes as permissões.

Podemos obsevar que temos projects e admin e ambos possuem alguns recursos semelhantes.

| project  | admin |
|----------|-------|
|![project-compute](./pics/project-compute.jpg)|![project-admin](./pics/project-admin.jpg)|

Os recursos criados em admin estão disponíveis para todos os projetos. Por exemplo temos flavors que são as defnições de máquinas que só está disponível em admin. Imagens por exemplo podem ser específicas de um projeto, mas se forem criadas dentro de admin estão disponíveis em todos os projetos.

Aqui uma amostra do que vem por padrão no devstack, mas podemos criar nossas próprias flavors que são definições de recursos de hardware específicos quando formos criar nossas máquinas.

![flavors](./pics/flavors.jpg)

O openstack pode virtualizar a máquina de diferentes maneiras. Na instalação do devstack por exemplo ele possui por default o QEMU como virtualizador, mas poderíamos instalar outros. Essa já é uma função do componente `Nova`.

![hypervisors](./pics/hypervisors.jpg)
![hypervisors-hosts](./pics/hypervisors-hosts.jpg)

Os hosts Aggregates são outros hosts que você poderia ter com openstack, mas ainda é cedo para falar desse assunto, pois precisariamos de uma instalação mais avançada.

A parte de volumes está dessa dashboard esta para o componentes `cinder`. Podemos criar um volume tanto no admin quando no project.

Em network temos a parter referente ao `neutron` quem é quem cuida da parte de redes. Mais pra frente veremos melhor sobre isso.

Outros componentes que fossem instalados iriam ganhando seus blocos dentro do `horizon`.

Na parte de system temos algumas informações de sistemas e quotas que poderíamos definir.

Na aba projects podemos observar que temos alguns recursos que não estão em admin pois são específicos de cada projeto, como `security groups` em networks e `key pairs` que são as chaves ssh que serão colocadas dentros da máquinas.
