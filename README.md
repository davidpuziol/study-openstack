# Openstack

![logo](./pics/openstack.svg)

[https://www.openstack.org/](https://www.openstack.org/)

## O que é o OpenStack?

O OpenStack é uma plataforma de computação em nuvem open source, projetada para fornecer uma infraestrutura escalável e flexível para ambientes de cloud privada. Assim com existe a AWS, GCP, Azure que são pública, o OpenStack é privado. A empresa compra o hardware (Servidor), coloca um sistema operacional e instala os componentes do Openstack que irá gerênciar os hardwares e oferecer um dashboard iterativo para criar os recursos como uma cloud.

Desenvolvido por uma comunidade global de colaboradores, o OpenStack oferece uma variedade de serviços essenciais para a construção e gestão de nuvens de maneira eficiente.

## Dica

Essa é uma ferramenta que, ou você sabe muito bem para reparar os problemas, ou é melhor nem aplicar. Já ouvi falar de muita empresas que deixaram de usar o openstack por não conseguir manter.

Estudar é bom para conhecer, mas é uma ferramenta que exige muito estudo.

## Como é o mundo OnPremisse tradicional?

Uma empresa que gosta de manter servidores on premisse geralmente possui um conjunto equipamentos que formam a infra.

Na parte de servidores geralmente é utilizado virtualização de Nível 1, ou seja, ocorre diretamente no hardware do servidor, sem a necessidade de um sistema operacional hospedeiro. Este tipo de virtualização utiliza um hipervisor bare metal, também conhecido como hipervisor de Type 1. Exemplos populares incluem VMware ESXi, Microsoft Hyper-V Server, KVM, ProxMox, Citrix, etc. Sendo assim quando uma máquina é necessária geralmente é criado um ticket de suporte para o tive de time que irá provisionar. Geralmente essas máquinas possuem bastante processador e memória.

Na parte de storage usa-se também servidores, mas com foco em armazenamento. A Dell, IBM, Hitachi e muitas outras possuem soluções de diversos tipos.

Para redes utilizamos os switches, load balancer, etc. Cisco, Juniper e Dell costumam ser as principais fornecedoras desse tipo de equipamento, mas temos uma vasta opção no mercado.

Se observar, temos 3 tipos de equipamentos e tudo que é necessário ser configurado nesses equipamentos por alguém.

Vamos imaginar que um desenvolver precisou de uma máquina para testar a aplicação e abriu o ticket para o time de infra. O que é necessário fazer?

- Subir uma vm usando uma iso do sistema operacional
- Ajustar configurações de rede se necessário
- Colocar a vm para armazenar os dados em uma SAN (Storage Area Network) para que os dados vão para os servidores de storage.

Que trabalheira... Isso é um gargalo!

## Infra As a Service (IaaS)

É um modelo de computação em nuvem que fornece recursos de infraestrutura virtualizados. Nesse modelo, a infraestrutura é disponibilizada como um serviço ou produto, permitindo obtenham recursos como servidores, armazenamento e redes, sem se preocupar com a gestão direta do hardware. Ou seja, estamos tentando eliminar o gargalo de ter alguém tendo que fazer as coisas.

Levando para exemplo do desenvolvedor que pediu a máquina, ele poderia simplesmente poderia fazer isso sozinho se tivesse como escolher a vm que precisa como um produto de prataleira, de forma mais rápida e ágil.

Na visão do time de infra o desenvolvedor seria o cliente.

## Como o OpenStack funciona?

Na verdade o openstack é um facilitador, que integra todos as solucões de infra que uma empresa possui, mas como serviço. O openstack não substituiria os servidores de virtualização, nem de storage e nem a parte de redes, mas ele conseguiria integrar todos eles para criar o IaaS e facilitar a entrega para o cliente.

A idéia é unificar tudo que você tem e entregar como serviço de forma fácil, rápida e iterativa.

![Alt text](./pics/image.png)

Quando cria uma vm já está integrando todos os componentes, já pega uma parte do storage, cria uma vlan, seleciona a imagem e entrega.

O Openstack faz tudo isso via api.

## Breve explicação sobre componentes

O openstack é um conjunto de componentes (serviços) que podem ou não estar ativos dependendo da sua instalação.

Não é necessário instalar todos os recursos que ele possui e por isso não é uma instalação simples só rodando um comando no terminal. É necessário subir os componentes e configurar de acordo com a necessidade.

## Principais Componentes

Alguns serviços são base e práticamente necessários, mas não obrigatório.

1. **Computação em Nuvem (Compute):**
   - O serviço Compute (Nova) permite o provisionamento e gerenciamento de máquinas virtuais, oferecendo poder computacional flexível para atender às demandas variáveis de carga de trabalho.

2. **Armazenamento em Nuvem (Storage):**
   - O OpenStack fornece serviços de armazenamento em bloco (Cinder) para volumes e armazenamento de objetos (Swift) para dados não estruturados, oferecendo soluções escaláveis e duráveis.

3. **Redes Definidas por Software (Networking):**
   - O serviço de Networking (Neutron) possibilita a criação e o gerenciamento de redes definidas por software, permitindo a construção de infraestruturas de rede personalizáveis e seguras.

4. **Autenticação e Autorização (Identity):**
   - O serviço de Identity (Keystone) gerencia a autenticação e autorização, garantindo o controle de acesso seguro aos recursos da nuvem.
     - Usuário
     - permissões
     - Roles
     - Grupos

5. **Dashboard de Usuário (Dashboard):**
   - O Horizon oferece uma interface gráfica para simplificar o gerenciamento e monitoramento da infraestrutura OpenStack, tornando as operações mais acessíveis. É possível controlar tudo pela linha de comando, mas também é possível via interface gráfica.

> Lembrando que não é necessário instalar componentes que não forem usar.

Os componentes opcionais do openstack podem ser encontrados em [https://www.openstack.org/software/project-navigator/openstack-components#openstack-services](https://www.openstack.org/software/project-navigator/openstack-components#openstack-services).

![Alt text](./pics/image-1.png)

### Flexibilidade e Interoperabilidade:

O OpenStack é altamente modular e oferece interoperabilidade com uma variedade de tecnologias e soluções, permitindo a integração com diferentes sistemas e facilitando a construção de ambientes de nuvem personalizados.

O openstack possui um provider para o opentofu e terraform podendo inclusive criar os recurso via Infra as a Code.

### Comunidade OpenStack:

Com uma comunidade ativa e diversificada de desenvolvedores, usuários e empresas, o OpenStack continua a evoluir e aprimorar suas funcionalidades. A colaboração aberta é uma pedra angular, impulsionando a inovação e mantendo o projeto alinhado com as necessidades em constante mudança do mundo da computação em nuvem.

Em resumo, o OpenStack é uma solução robusta para quem busca construir e gerenciar infraestruturas de nuvem, proporcionando flexibilidade, escalabilidade e controle em um ambiente de código aberto.
