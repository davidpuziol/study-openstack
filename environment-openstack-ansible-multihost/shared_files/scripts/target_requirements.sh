#!/bin/bash

echo "Install Requirement Packages for Targets"
sudo apt-get install -y bridge-utils debootstrap openssh-server tcpdump vlan python3
sudo apt-get install linux-modules-extra-$(uname -r) -y

echo "Rebooting"
sudo reboot