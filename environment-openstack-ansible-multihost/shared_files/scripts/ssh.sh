#!/bin/bash

echo "Add Personal SSH Keys to Access Vagrant and Ubuntu"
tee -a /home/vagrant/.ssh/authorized_keys < /vagrant/shared/openstack.pub
cp /vagrant//shared/openstack /home/vagrant/.ssh/id_rsa
chmod 400 /home/vagrant/.ssh/id_rsa

sudo tee -a /home/ubuntu/.ssh/authorized_keys < /vagrant/shared/openstack.pub
sudo cp /vagrant/shared/openstack /home/ubuntu/.ssh/id_rsa
sudo chown ubuntu:ubuntu /home/ubuntu/.ssh/id_rsa
sudo chmod 400 /home/ubuntu/.ssh/id_rsa

sudo tee -a /root/.ssh/authorized_keys < /vagrant/shared/openstack.pub