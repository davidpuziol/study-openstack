#!/bin/bash

echo "Install Requirement Packages for Installer"
sudo apt-get install -y build-essential git chrony openssh-server python3-dev sudo

echo "Get Openstack Ansible and Prepare Environment"
sudo git clone -b stable/2023.2 https://github.com/openstack/openstack-ansible.git /opt/openstack-ansible; cd /opt/openstack-ansible
sudo scripts/bootstrap-ansible.sh

sudo cp -r /opt/openstack-ansible/etc/openstack_deploy/ /etc/openstack_deploy; cd /etc/openstack_deploy
# sudo cp openstack_user_config.yml.example openstack_user_config.yml