#!/bin/bash

# Lista dos diretórios dos seus projetos
projetos=("compute_machines" "openstack_machines" "storage_machines" "installer_machine")

# Loop para destruir as máquinas Vagrant em todos os projetos
for projeto in "${projetos[@]}"
do
    cd "$projeto" || exit 1
    vagrant destroy --force
    cd ..
done
