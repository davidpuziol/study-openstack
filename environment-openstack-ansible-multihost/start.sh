#!/bin/bash

# Lista dos diretórios dos seus projetos
projetos=("compute_machines" "openstack_machines" "storage_machines" "installer_machine")

vagrant plugin install vagrant-disksize

# Loop para iniciar o Vagrant em paralelo para cada projeto
for projeto in "${projetos[@]}"
do
    cd "$projeto" || exit 1
    vagrant up
    cd ..
done
