# Instalação

O Openstack é extremamente complexo de instalar e existem várias formas de instalar dependendo da finalidade:

## Projetos de Instalação

1. **[DevStack](https://docs.openstack.org/devstack/latest/):**
   - [github DevStack](https://opendev.org/openstack/devstack)
   - O DevStack é uma opção popular para ambientes de desenvolvimento e teste.
   - Suporta as duas últimas versões do Ubuntu LTS, Rocky Linux 9
   - Pode ser instalado em uma única máquina virtual ou em hardware dedicado.
   - É fácil de configurar e é adequado para quem deseja explorar o OpenStack rapidamente.

2. **[Packstack RDO](https://www.rdoproject.org/install/packstack/):**
   - [github Packstack](https://github.com/redhat-openstack/packstack)
   - O Packstack é um conjunto de scripts de instalação para o OpenStack que simplifica o processo.
   - Pode ser usado para instalações rápidas em máquinas CentOS ou RHEL.
   - É útil para ambientes de teste (prova de conceito) e desenvolvimento

3. **[OpenStack-Ansible](https://docs.openstack.org/openstack-ansible/latest/):**
   - [github Openstack Ansible](https://github.com/openstack/openstack-ansible)
   - Utiliza o Ansible para automatizar a implantação do OpenStack.
   - Oferece maior flexibilidade e é adequado para ambientes de produção.
   - Pode ser personalizado para atender a requisitos específicos.

4. **Kolla:**
   - [github Kolla](https://github.com/openstack/kolla)
   - Usa contêineres Docker e Kubernetes para implantar serviços OpenStack.
   - Oferece uma abordagem moderna e modular para a implantação.

5. **[Microstack](https://microstack.run/):**
   - Deploy o openstack em um cluster microk8s
   - Utiliza o juju e o ubuntu
   - Utilizado para testes, desenvolvimento e uso local
   - Oferece uma abordagem moderna e modular para a implantação.

6. **Charms:**
   - O projeto OpenStack Charms utiliza o Juju, um serviço de orquestração, para instalar e gerenciar serviços OpenStack.
   - Pode ser uma opção eficiente para ambientes de produção, especialmente em nuvens com várias máquinas.
   - Projeto da canonical

7. **TripleO (OpenStack on OpenStack):**
   - Uma abordagem que utiliza o OpenStack para implantar e gerenciar outras instâncias do OpenStack.
   - Pode ser complexo, mas oferece flexibilidade e é adequado para ambientes empresariais.

8. **Mirantis OpenStack:**
   - Uma distribuição do OpenStack fornecida pela Mirantis, que simplifica a implantação e gerenciamento.
   - Pode ser uma escolha para organizações que desejam suporte comercial.

Ao escolher a melhor abordagem, considere a complexidade do seu ambiente, os recursos disponíveis, os requisitos de produção e o nível de personalização desejado. O OpenStack tem uma comunidade ativa que fornece documentação abrangente para cada método de instalação. Certifique-se de consultar a documentação oficial correspondente à versão específica do OpenStack que você está instalando.

Podemos ir tentando diferentes instalações e ir crescendo ao longo do caminho até chegar em uma instalação de produtiva.