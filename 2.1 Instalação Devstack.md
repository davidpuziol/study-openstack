# Instalação Devstack

Acredito que essa é a instalação por onde podemos começar a explorar o openstack e entender os componentes.

A instalação com o devstack é para ambientes de testes. No nosso projeto vamos criar somente uma máquina que será responsável por todos os recursos necessários.
Vamo utilizar o vagrant com o virtualbox para criar uma máquina onde instalaremos o openstack.

É necessário ter o virtualbox que será o nosso provisionador e o vagrant que fará as configurações da vm.

```bash
sudo apt-get install virtualbox
sudo apt-get install vagrant
vagrant --version
Vagrant 2.4.0
```

Vamos subir uma máquina virtual usando o vagrant, e para isso é necessário vagrant instalado e virtualbox para ser o provisioner do vagrant.
É importante observar que a minha placa de rede que eu estou recebendo endereço ip é `wlo1` veja a sua com o comando `ip addr show`.

A máquina que estamos criando será um ubuntu 22.04 lts com 10 processadores, 10G de ram e 60 de HD.

Depois de criada, o vagrant acessará essa máquina via ssh e rodará o script `requirements.sh` que irá criar um usuário openstack e sua home em /opt e baixar o projeto devstack na versão específica estável dentro dessa pasta.

Depois disso iremos rodar o start.sh que criará o arquivo local.conf com as configurações passadas ao devstack de nossa preferência. dentro do projeto clonado e executar o stash.

O projeto se encontra em [https://gitlab.com/davidpuziol/study-openstack/-/tree/main/environment-devstack](https://gitlab.com/davidpuziol/study-openstack/-/tree/main/environment-devstack)

```bash
Vagrant.require_version ">= 2.4.0"

Vagrant.configure("2") do |config|
    config.vm.box = "ubuntu/jammy64"
    config.vm.hostname = "devstack"
   # Mude a sua interface de rede
    config.vagrant.plugins = "vagrant-disksize"
    config.disksize.size = '60GB'
    config.vm.network "public_network", bridge: "wlo1"
    config.vm.provider "virtualbox" do |vb|
        vb.name = "devstack"
        vb.memory = 10240
        vb.cpus = 10
   end
   config.vm.provision "shell", path: "requirements.sh"
   config.vm.provision "shell", path: "start.sh"
end
```

Uma vez que a máquina subiu vamos iniciar a instalação rodando o script stack.sh que usará as configurações de local.conf para instalar o openstack. Esse passo manual não foi coloado no script start.sh para evitar que façamos a instalação toda vez que subirmos a máquina.

```bash
vagrant ssh
sudo -u stack -i
cd devstack
./stack.sh
...
...
...
This is your host IP address: 10.0.0.249
This is your host IPv6 address: ::1
Horizon is now available at http://10.0.0.249/dashboard
Keystone is serving at http://10.0.0.249/identity/
The default users are: admin and demo
The password: admin

Services are running under systemd unit files.
For more information see: 
https://docs.openstack.org/devstack/latest/systemd.html

DevStack Version: 2023.2
Change: b082d3fed3fe05228dabaab31bff592dbbaccbd9 Make multiple attempts to download image 2023-12-12 08:07:39 +0000
OS Version: Ubuntu 22.04 jammy
```

Para desligar a máquina poderíamos fazer o comando `vagrant halt` e para subir novamente poderíamos usar o comando `vagrant up --no-provision` para não rodar os scripts.
