# Architetura

O Open stack é um pouco dificil de entender no começo...

Primeiro é necessário entender o que cada componentes faz

![map](./pics/openstack-map.png)

Também é possível observar que criar recursos no openstack podem ser de várias maneiras, via interface grafíca, sdk, terraform, manifestos do kubernetes, etc, assim como em um cloud.

![diagram](./pics/software-overview-diagram.svg)

Para se comunicar com o openstack temos um CLI (assim com awscli, e az), o Horizon que é um compenent gráfico que não é obrigatório a instalação, providers e SKDs usadas para desenvolvimento.

## Openstack CLI

Obviamente que o openstack cli irá precisar se comunicar e apontar para o servidor openstack para logar e criar os recursos.

Para instalar a cli é necessário o python instalado. A documentação mostra exatamente como fazer.

```bash
# Se nào tiver o python
https://pypi.org/project/python-openstackclient/

curl https://pyenv.run | bash

# se usar o bash mude .zsrhc para .bashrc
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.zshrc
echo '[[ -d $PYENV_ROOT/bin ]] && export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.zshrc
echo 'eval "$(pyenv init -)"' >> ~/.zshrc
```

Usando o pyenv você pode encontrar as mais recentes versões do python.

```bash
pyenv install --list
# Para pegar a ultima versão
pyenv install --list | grep -v - | grep -v - b| tail-1
# Para instalar diretamente aultima versão
pyenv install $(pyenv install --list | grep -v - | grep -v b | tail -1)

python3 -V
Python 3.10.12


#sudo apt-get install python3-pip    
python3 -m pip install --upgrade pip

# Instalando o CLI
pip install python-openstackclient
echo 'export PATH="$HOME/.local/bin:$PATH"' >> ~/.zshrc

openstack --version         
openstack 6.4.0
```
